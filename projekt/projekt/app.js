const form = document.getElementById('form');
const towar = document.getElementById('towar');
const kod = document.getElementById('kod');
const netto = document.getElementById('netto');
const vat = document.getElementById('vat');
const brutto = document.getElementById('brutto');
const kategoria = document.getElementById('kategoria');

const opcjaTowaru1 = document.getElementById('opcjaTowaru1');
const opcjaTowaru2 = document.getElementById('opcjaTowaru2');
const opcjaTowaru3 = document.getElementById('opcjaTowaru3');
const opcjaTowaru4 = document.getElementById('opcjaTowaru4');
const opcjaTowaru5 = document.getElementById('opcjaTowaru5');

const ocena1 = document.getElementById('ocena1');
const ocena2 = document.getElementById('ocena2');
const ocena3 = document.getElementById('ocena3');
const ocena4 = document.getElementById('ocena4');
const ocena5 = document.getElementById('ocena5');

const zdjecie = document.getElementById('zdjecie');

const effect = document.getElementById('effects');

let stageOfNetto = false;

function showError(input) {
    const formControl = input.nextElementSibling;
    input.className = 'form-control is-invalid';
    formControl.textContent = "Zle wypelnione pole.";
    formControl.className = "invalid-feedback";
    formControl.style.display = "block";
}

function showSuccess(input) {
    const formControl = input.nextElementSibling;
    input.className = 'form-control is-valid';
    formControl.textContent = "Dobrze !"
    formControl.className = "valid-feedback";
    formControl.style.display = "block";
}

function checkTowar(input, min) {
    if (!(input.value.length > 10) && input.value.length > 0) {
        if (input.value.match(/^[A-Za-z]+$/)) {
            showSuccess(input);
        } else {
            showError(input);
        }
    } else {
        showError(input);
    }
}

function checkKod(input) {
    if (input.value.length > 0) {
        if (input.value.match(/^\w{2}-\w{2}$/)) { 
            showSuccess(input);
        } else {
            showError(input);
        }
    } else {
        showError(input);
    }
}


function checkNetto(input) {
    if (input.value.length > 0) {
        if (input.value.match(/^\d+(\.\d{1,2})?$/)) {
            showSuccess(input);
            if (input.value.indexOf(".") === -1) {
                input.value = `${input.value}.00`;
            }
            stageOfNetto = true;
            return true;
        } else {
            stageOfNetto = false;
            showError(input);
        }
    } else {
        stageOfNetto = false;
        showError(input);
    }
}

function checkVAT(input) {
    if (input.value.length > 0) {
        if (input.value.match(/^\d+$/)) {
            showSuccess(input);
            return true;
        } else {
            showError(input);
        }
    } else {
        showError(input);
    }
}

function checkBrutto(input) {
    if (stageOfNetto && checkVAT(vat)) {
        const netto2 = parseFloat(netto.value);
        const vat2 = parseFloat(vat.value);
        const sum = netto2 + (netto2 * vat2 / 100);
        input.value = `${sum}`;
    }
}

function checkKategoria(input) {
    if (input.value === "Wybierz...") {
        showError(input);
    } else {
        showSuccess(input);
    }
}

function checkOpcjaTowaru(ArrInput) {
    let sum = 0;
    ArrInput.forEach(function(item) {
        if (item.checked) {
            sum++;
        }
    });
    if (sum >= 2) {
        const elem = document.getElementById('lastTowar').nextElementSibling;
        elem.textContent = "Dobrze !"
        elem.className = "valid-feedback";
        elem.style.display = "block";
    } else {
        const elem = document.getElementById('lastTowar').nextElementSibling;
        elem.textContent = "Zle wypelnione pole."
        elem.className = "invalid-feedback";
        elem.style.display = "block";
    }
}

function checkOcena(ArrInput) {
    let stage = false;
    ArrInput.forEach(function(item) {
       if (item.checked == true) {
            const elem = document.getElementById('lastOcena').nextElementSibling;
            elem.textContent = "Dobrze !"
            elem.className = "valid-feedback";
            elem.style.display = "block";
            stage = true;
       }
    });
    if (stage)
        return true;
    
    const elem = document.getElementById('lastOcena').nextElementSibling;
    elem.textContent = "Dobrze !"
    elem.textContent = "Zle wypelnione pole."
    elem.className = "invalid-feedback";
    elem.style.display = "block";
}

function checkZdjecie(input) {
    if (input.value.length > 0) {
        showSuccess(input)
    } else {
        showError(input);
    }
}

towar.addEventListener('blur', function() {
    checkTowar(towar);
});

kod.addEventListener('blur', function() {
    checkKod(kod);
});

netto.addEventListener('blur', function() {
    checkNetto(netto);
});

vat.addEventListener('blur', function() {
    checkVAT(vat);
});

kategoria.addEventListener('blur', function() {
    checkKategoria(kategoria);
});

effect.addEventListener('blur', function() {
    checkKategoria(kategoria);
});

zdjecie.addEventListener('blur', function() {
    checkZdjecie(zdjecie);
});

opcjaTowaru1.addEventListener('change', function() {
    checkOpcjaTowaru([opcjaTowaru1, opcjaTowaru2, opcjaTowaru3, opcjaTowaru4, opcjaTowaru5]);
});

opcjaTowaru2.addEventListener('change', function() {
    checkOpcjaTowaru([opcjaTowaru1, opcjaTowaru2, opcjaTowaru3, opcjaTowaru4, opcjaTowaru5]);
});

opcjaTowaru3.addEventListener('change', function() {
    checkOpcjaTowaru([opcjaTowaru1, opcjaTowaru2, opcjaTowaru3, opcjaTowaru4, opcjaTowaru5]);
});

opcjaTowaru4.addEventListener('change', function() {
    checkOpcjaTowaru([opcjaTowaru1, opcjaTowaru2, opcjaTowaru3, opcjaTowaru4, opcjaTowaru5]);
});

opcjaTowaru5.addEventListener('change', function() {
    checkOpcjaTowaru([opcjaTowaru1, opcjaTowaru2, opcjaTowaru3, opcjaTowaru4, opcjaTowaru5]);
});

ocena1.addEventListener('change', function() {
    checkOcena([ocena1, ocena2, ocena3, ocena4, ocena5]);
});

ocena2.addEventListener('change', function() {
    checkOcena([ocena1, ocena2, ocena3, ocena4, ocena5]);
});

ocena3.addEventListener('change', function() {
    checkOcena([ocena1, ocena2, ocena3, ocena4, ocena5]);
});

ocena4.addEventListener('change', function() {
    checkOcena([ocena1, ocena2, ocena3, ocena4, ocena5]);
});

ocena5.addEventListener('change', function() {
    checkOcena([ocena1, ocena2, ocena3, ocena4, ocena5]);
});

form.addEventListener('submit', function(e) {
    e.preventDefault();
    checkTowar(towar);
    checkKod(kod);
    checkNetto(netto);
    checkVAT(vat);
    checkBrutto(brutto);
    checkKategoria(kategoria);
    checkOpcjaTowaru([opcjaTowaru1, opcjaTowaru2, opcjaTowaru3, opcjaTowaru4, opcjaTowaru5]);
    checkOcena([ocena1, ocena2, ocena3, ocena4, ocena5]);
    checkZdjecie(zdjecie);
});


